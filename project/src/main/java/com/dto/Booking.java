package com.dto;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor

@Entity
@Table(name="booking")
public class Booking {
	@Id@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int bookingId;

	private int userId;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern="dd-MM-yyyy")
	private Date bookingDate;

	@OneToOne(targetEntity = User.class)
	@JoinColumn(name =" userId" ,insertable=false, updatable=false)
	private User user;

	@OneToOne(targetEntity = ServiceEvent.class)
	@JoinColumn(name = "event_id", referencedColumnName = "eventId", insertable = false, updatable = false)
	private ServiceEvent serviceevent;

	public Booking() {
		super();
	}

	public Booking(int bookingId, int userId, int eventid, Date bookingDate, User user, ServiceEvent serviceevent) {
		super();
		this.bookingId = bookingId;
		this.userId = userId;
		this.bookingDate = bookingDate;
		this.user = user;
		this.serviceevent = serviceevent;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public Date getBookingDate() {
		return bookingDate;
	}

	public void setBookingDate(Date bookingDate) {
		this.bookingDate = bookingDate;
	}

	public int getBookingId() {
		return bookingId;
	}

	public void setBookingId(int bookingId) {
		this.bookingId = bookingId;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public ServiceEvent getServiceevent() {
		return serviceevent;
	}

	public void setServiceevent(ServiceEvent serviceevent) {
		this.serviceevent = serviceevent;
	}

	@Override
	public String toString() {
		return "Booking [bookingId=" + bookingId + ", userId=" + userId + ", bookingDate="
				+ bookingDate + ", user=" + user + ", serviceevent=" + serviceevent + "]";
	}

}
