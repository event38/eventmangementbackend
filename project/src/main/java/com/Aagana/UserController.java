package com.Aagana;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.UserDao;
import com.dto.User;

@RestController
//@CrossOrigin(origins = "http://localhost:4200")

public class UserController {
	private static final BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
	@Autowired
	UserDao userDao;

	@Autowired
	private SmsService smsService;

	public UserController (UserDao userDao) {
		this.userDao = userDao;
	}

	@GetMapping("/displayAllUsers")
	public List<User> getAllUsers() {
		return userDao.displayAllUsers();
	}

	@GetMapping("/getUserByName/{userName}") 
	public User getUserByName(@PathVariable("userName") String UserName) { 
		return userDao.getUserByUserName(UserName);
	}

	@PostMapping("/registerUser")
	public User registerUser(@RequestBody User user) {
		String EmailId = user.getEmailId();
		System.out.println(EmailId);
		String subject = "register";
		String message = "Welcome to Aagana Event Management," + "ThankYou for Joining this origanization";
		EmailService.sendEmail(message, subject, EmailId, "purushothammaddi120@gmail.com");
		User reguser = new User(0, user.getUserName(), user.getFullName(), user.getEmailId(), user.getPhoneNo(),
				user.getencodedPassword(), null);
		return userDao.insert(reguser);

	}

	@GetMapping("/getUserById/{userId}")
	public User getUserById(@PathVariable("userId") int userId) {
		return userDao.getUserById(userId);
	}

	@PutMapping("/updateUser")
	public User updateUser(@RequestBody User user) {
		return userDao.insert(user);
	}

	@GetMapping("/login/{emailId}/{password}")
	public boolean userAuthentication(@PathVariable("emailId") String emailId,
			@PathVariable("password") String password) {
		String encodedPassword = userDao.userAuthentication(emailId);
		return passwordEncoder.matches(password, encodedPassword);
	}

	@GetMapping("/send-sms/{phoneNo}")
	public void sendSms(@PathVariable("phoneNo") String phoneNo) {
		int min=100000;
		int max=999999;
		int otp=(int)(Math.random()*(max-min+1)+min);
		String msg="your OTP is"+otp+"please verify this in your application";
		System.out.println(otp);
		String ser = smsService.sendSms(phoneNo, msg);
		if(ser != null) {
			System.out.println( otp);
		}
		else {
			System.out.println();
		}
	}

	@GetMapping("/sendOTPByEmail/{emailId}")
	public int sendOTPByEmail(@PathVariable("emailId") String emailId) {
		int min=1000000;
		int max=999999;
		int otp =(int) (Math.random()*(max-min +1) +min);
		String msg="your OTP is  "+ otp +" please verify this in your email Application";
		System.out.println(otp);
		String subject ="otp service";
		boolean serv=EmailService.sendEmail(msg,subject,emailId,"purushothammaddi120@gmail.com");
		if(serv) {
			return otp;
		}
		else {
			return 0;
		}
	}


	@PutMapping("/updatePassword")
	public int updatePass(@RequestBody User user1) {
		String phoneNo=user1.getPhoneNo();
		String password=user1.getencodedPassword();
		System.out.println(phoneNo);
		System.out.println(password);
		return userDao.setPass(phoneNo, password);
	}

	@PutMapping("/updateNewPass")
	public int newPass(@RequestBody User user) {
		String emailId=user.getEmailId();
		String password=user.getPassword();
		return userDao.setNewPass(emailId, password);
	}
}