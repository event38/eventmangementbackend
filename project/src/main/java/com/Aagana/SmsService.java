package com.Aagana;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;

import com.twilio.Twilio;
import com.twilio.exception.ApiException;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Configuration
@Data
@Getter
@Setter

@Service
public class SmsService {
	private final String twilioAccountSId;
	private final String twilioAuthToken;
	private final String twilioPhoneNumber;

	public SmsService(@Value("${twilio.account.sid}") String twilioAccountSId,
			@Value("${twilio.auth.token}") String twilioAuthToken,
			@Value("${twilio.phone.number}") String twilioPhoneNumber) {
		this.twilioAccountSId = twilioAccountSId;
		this.twilioAuthToken = twilioAuthToken;
		this.twilioPhoneNumber = twilioPhoneNumber;

		Twilio.init(twilioAccountSId, twilioAuthToken);

	}

	public  String sendSms(String phoneNo, String msg) {
		boolean flag=true;
		try {
			Message.creator(new PhoneNumber(phoneNo),
					new PhoneNumber(twilioPhoneNumber), msg).create();
		}catch(ApiException e) {
			e.printStackTrace();
			flag = false;
		}
		if(flag) {
			return msg;
		}
		else {
			return null;
		}

	}

	public String getTwilioAuthToken() {
		return twilioAuthToken;
	}

	public String getTwilioAccountSId() {
		return twilioAccountSId;
	}

}
