package com.Aagana;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.ServiceEventDao;
import com.dto.ServiceEvent;

@RestController
public class ServiceEventController {
	@Autowired
	ServiceEventDao serviceDao;

	@GetMapping("/getAllEvents")
	public List<ServiceEvent> displayAllEvents(){
		return serviceDao.displayAllEvents();
	}

	@GetMapping("/getEventById/{eventId}")
	public ServiceEvent getUserById(@PathVariable("eventId") int eventId) {
		return serviceDao.selectEventById(eventId);
	}
	@GetMapping("/getEventByName/{eventName}")
	public ServiceEvent getEventByName(@PathVariable("eventName") String eventName) {
		return serviceDao.getEventByName(eventName);
	}

	@PostMapping("/registerEvent")
	public void registerEvent(@RequestBody ServiceEvent event) {
		serviceDao.addEvent(event);
	}

	@PutMapping("updateEvent")
	public void updateEvent(@RequestBody ServiceEvent event) {
		serviceDao.addEvent(event);
	}

	@DeleteMapping("deleteEventId/{eventId}")
	public String deleteEvent(@PathVariable("eventId") int eventId) {
		serviceDao.deleteEvent(eventId);
		return "Service Event Deleted Successfully";
	}

	@GetMapping("getImages/{eventId}")
	public List<ServiceEvent> getEventById(@PathVariable("eventId") int eventId){
		return serviceDao.findAllEventid(eventId);
	}

	@GetMapping("getEventByLocation/{eventLocation}")
	public List<ServiceEvent> geteventsByLocation(@PathVariable("eventLocation") String eventLocation){
		return serviceDao.findAllEventsByLocation(eventLocation);
	}

}
