package com.Aagana;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.FeedbackDao;
import com.dto.Feedback;

@RestController
public class FeedbackController {

	@Autowired
	FeedbackDao feedbkDao;

	public FeedbackController(FeedbackDao feedbkDao) {
		this.feedbkDao=feedbkDao;
	}

	@PostMapping("addFeedback")
	public void addFeedback(@RequestBody Feedback feed) {
		feedbkDao.addFeedback(feed);
	}

	@GetMapping("displayAllFeedback")
	public List<Feedback> displayAllFeedbacks(){
		return feedbkDao.displayFeedback();
	}
}
