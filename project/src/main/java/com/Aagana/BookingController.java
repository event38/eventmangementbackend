package com.Aagana;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.BookingDao;
import com.dto.Booking;

@RestController
public class BookingController {

	@Autowired
	BookingDao bkDao;

	public BookingController(BookingDao bkDao) {
		this.bkDao=bkDao;
	}

	@PostMapping("addBooking")
	public void addBooking(@RequestBody Booking book) {
		bkDao.register(book);
	}

	@GetMapping("displayAllBooking")
	public List<Booking> displayAllBookingEvents(){
		return bkDao.displayAllBookings();
	}

	@DeleteMapping("deleteBookingById/{bookingId}")
	public void deleteBooking(@PathVariable("bookingId") int bookingId) {
		bkDao.removeBookingById(bookingId);
	}

	@PutMapping("updateBooking")
	public void updatBooking(@RequestBody Booking book) {
		bkDao.updateBooking(book);
	}

}
