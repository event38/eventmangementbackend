package com.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dto.ServiceEvent;

@Service
public class ServiceEventDao {
	@Autowired
	private ServiceEventRepository serviceEventRepos;

	//Getting the events names
	@Transactional
	public List<ServiceEvent> displayAllEvents(){
		return serviceEventRepos.findAll();
	}

	//getting the event id 
	@Transactional
	public ServiceEvent selectEventById(int eventId){
		return serviceEventRepos.findById(eventId).orElse(new ServiceEvent(1,"event name not found","ownername not found","owner email NA",0,0,"servicetype NA","eventlocation NA","eventImage NA","image1 NA","Image2 NA","review NA","desc NA"));
	}

	//add the events
	@Transactional
	public void addEvent(ServiceEvent event) {
		serviceEventRepos.save(event);
	}

	//delete the events
	@Transactional
	public void deleteEvent(int eventId) {
		serviceEventRepos.deleteById(eventId);
	}
	
	//update the events
	@Transactional
	public void updateEvents(ServiceEvent event) {
		serviceEventRepos.save(event);
	}

	//getting the event names
	@Transactional
	public ServiceEvent getEventByName(String eventName) {
		ServiceEvent servevent =serviceEventRepos.getEventByName(eventName);
		if(servevent != null) {
			return servevent;
		}
		return new ServiceEvent(1,"event name not found","ownername not found","owner email NA",0,0,"servicetype NA","eventlocation NA","eventImage NA","image1 NA","Image2 NA","review NA","desc NA");
	} 
	
	@Transactional
	public List<ServiceEvent> findAllEventid(int eventId) {
		// TODO Auto-generated method stub
		return serviceEventRepos.findAllEventid(eventId);
	}
	
	@Transactional
	public List<ServiceEvent> findAllEventsByLocation(String eventLocation) {
		// TODO Auto-generated method stub
		return serviceEventRepos.findAllEventsByLocation(eventLocation); 	}
	



}
