package com.dao;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.dto.User;
public interface UserRepository extends JpaRepository<User, Integer>{

	
	@Transactional
	@Modifying
	@Query("from User u where u.userName = :userName")
	public User getUserByName(@Param("userName") String userName);
	
	@Transactional
	@Modifying
	@Query("select e.password from User e where e.userName = :userName")
	public String userAuthentication(@Param("userName")String userName);
	
	@Transactional
	@Modifying
	@Query("UPDATE User e SET e.password= :password WHERE e.phoneNo =:phoneNo")
	public int updatePass(@Param("phoneNo") String phoneNo,@Param("password") String password);
	
	@Transactional
	@Modifying
	@Query("UPDATE User e SET e.password =:password WHERE e.emailId =:emailId")
	public int updateNewPass(@Param("emailId")String emailId,@Param("password") String password);

}
