package com.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.dto.User;

@Service
public class UserDao {
	@Autowired
	UserRepository userRepos;

	@Transactional
	public List<User> displayAllUsers(){
		return userRepos.findAll();
	}

	@Transactional
	public User registerUser(User user) {

		String password =user.getPassword();
		BCryptPasswordEncoder passwordEncoder=new BCryptPasswordEncoder();
		String hashedPassword =passwordEncoder.encode(password);
		user.setPassword(hashedPassword);
		return userRepos.save(user);
	}

	@Transactional
	public User getUserById(int userId) {

		return userRepos.findById(userId).get();
	}

	@Transactional
	public User insert(User user) {
		return userRepos.save(user);
	}

	@Transactional
	public String userAuthentication(String userName) {
		return userRepos.userAuthentication(userName);
	}

	@Transactional
	public User getUserByUserName(String userName) {
		return userRepos.getUserByName(userName);
	}

	@Transactional
	public int setPass(String phoneNo,String password) {
		return userRepos.updatePass(phoneNo,password);
	}

	@Transactional
	public int setNewPass(String emailId, String password) {
		return userRepos.updateNewPass(emailId,password);
	}

}

