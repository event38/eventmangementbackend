package com.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.dto.Booking;

public interface BookingRepository extends JpaRepository<Booking, Integer>{

}
